package com.fabrick.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fabrick.demo.models.cash.AccountTransaction;

public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, String> {

}
