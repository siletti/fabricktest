package com.fabrick.demo.models.cash;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AccountTransaction {
	
	@Id
	private String transactionId;
	private String operationId;
	private String accountingDate;
	private String valueDate;
	@Embedded
	private TransactionType type;
	private String amount;
	private String currency;
	private String description;

}
