package com.fabrick.demo.models.cash;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountBalancePayload {

	private String date;
	private String balance;
	private String availableBalance;
	private String currency;

}