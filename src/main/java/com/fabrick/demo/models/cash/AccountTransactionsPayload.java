package com.fabrick.demo.models.cash;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountTransactionsPayload {
	
	private List<AccountTransaction> list;

}
