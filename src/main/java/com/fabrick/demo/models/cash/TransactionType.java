package com.fabrick.demo.models.cash;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class TransactionType {
	
	private String enumeration;
	private String value;

}
