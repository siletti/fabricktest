package com.fabrick.demo.models.common;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error {

	private String code;
	private String description;
	private List<Object> params = null;
	
}
