package com.fabrick.demo.models.common;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralResponse<T>  {

	private String status;
	private List<Error> errors;
	private  T payload;
    
}
