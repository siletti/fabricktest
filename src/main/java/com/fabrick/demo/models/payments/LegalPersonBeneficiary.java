package com.fabrick.demo.models.payments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LegalPersonBeneficiary {

	private String fiscalCode;
	private String legalRepresentativeFiscalCode;

}
