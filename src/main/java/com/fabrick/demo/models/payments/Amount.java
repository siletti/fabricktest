package com.fabrick.demo.models.payments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Amount {

	private String debtorAmount;
	private String debtorCurrency;
	private String creditorAmount;
	private String creditorCurrency;
	private String creditorCurrencyDate;
	private String exchangeRate;

}
