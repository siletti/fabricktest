package com.fabrick.demo.models.payments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fee {

	private String feeCode;
	private String description;
	private String amount;
	private String currency;

}
