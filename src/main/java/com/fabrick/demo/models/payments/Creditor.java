package com.fabrick.demo.models.payments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Creditor {
	
	private String name;
	private Account account;
	private Address address;

}
