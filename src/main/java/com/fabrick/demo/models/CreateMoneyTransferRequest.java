package com.fabrick.demo.models;

import com.fabrick.demo.models.payments.CreateMoneyTransferRqBody;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateMoneyTransferRequest {
	
	private long idAccount; 
	private String headerXTimeZone;
	private CreateMoneyTransferRqBody body;

}
