package com.fabrick.demo.models;

import com.fabrick.demo.models.cash.AccountBalancePayload;
import com.fabrick.demo.models.common.GeneralResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountBalanceResponse {
	private GeneralResponse<AccountBalancePayload> response;
}
