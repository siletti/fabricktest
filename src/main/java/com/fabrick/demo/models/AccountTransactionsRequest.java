package com.fabrick.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountTransactionsRequest {
	private long accountId;
	private String fromAccountingDate;
	private String toAccountingDate;
}
