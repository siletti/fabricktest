package com.fabrick.demo.models;

import com.fabrick.demo.models.common.GeneralResponse;
import com.fabrick.demo.models.payments.CreateMoneyTransferPayload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateMoneyTransferResponse {
	private GeneralResponse<CreateMoneyTransferPayload> response;

}
