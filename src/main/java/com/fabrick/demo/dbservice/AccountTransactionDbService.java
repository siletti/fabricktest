package com.fabrick.demo.dbservice;

import java.util.List;

import com.fabrick.demo.models.cash.AccountTransaction;

public interface AccountTransactionDbService {
	public List<AccountTransaction> findAll();
	public AccountTransaction findById(String theId);
	public void save(AccountTransaction theEmployee);
	public void deleteById(String theId);

}
