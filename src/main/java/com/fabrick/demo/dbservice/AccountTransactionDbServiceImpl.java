package com.fabrick.demo.dbservice;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.fabrick.demo.dao.AccountTransactionRepository;
import com.fabrick.demo.models.cash.AccountTransaction;

@Service
public class AccountTransactionDbServiceImpl implements AccountTransactionDbService {
	
	private AccountTransactionRepository repository;
	
	public AccountTransactionDbServiceImpl(AccountTransactionRepository accountTransactionRepository) {
		this.repository = accountTransactionRepository;
	}

	@Override
	public List<AccountTransaction> findAll() {
		return repository.findAll();
	}

	@Override
	public AccountTransaction findById(String theId) {
		Optional<AccountTransaction> result = repository.findById(theId);
		return result.orElse(null);
	}

	@Override
	public void save(AccountTransaction theEmployee) {
		repository.save(theEmployee);
	}

	@Override
	public void deleteById(String theId) {
		repository.deleteById(theId);
	}

}
