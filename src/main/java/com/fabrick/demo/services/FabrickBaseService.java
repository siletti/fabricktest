package com.fabrick.demo.services;

import java.time.Duration;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fabrick.demo.models.common.GeneralResponse;

@Service
@PropertySource("classpath:fabrick.properties")
public abstract class FabrickBaseService<REQ, RES> {
	
	private Logger log = LoggerFactory.getLogger(FabrickBaseService.class);
	
	@Value("${fabrick.base_path}")
	private String basePath;
	@Value("${fabrick.accounts_path}")
	private String accountsPath;
	@Value("${fabrick.balance_path}")
	private String baseBalancePath;
	@Value("${fabrick.transactions_path}")
	private String baseTransactionsPath;
	@Value("${fabrick.transfer_path}")
	private String baseTransferPath;
	@Value("${fabrick.Auth-Schema}")
	private String authSchema;
	@Value("${fabrick.Api-Key}")
	private String apiKey;
	
	private  RestTemplate restTemplate;

	public FabrickBaseService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.setConnectTimeout(Duration.ofSeconds(30))
				.setReadTimeout(Duration.ofSeconds(30)).build();
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
	
	public String getBalancePath() {
		return basePath + accountsPath + baseBalancePath;
	}
	
	public String getTransactionsPath() {
		return basePath + accountsPath + baseTransactionsPath;
	}

	public String getmoneyTransferPath() {
		return  basePath + accountsPath + baseTransferPath;
	}
	
	public HttpHeaders setHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Auth-Schema", authSchema);
		headers.set("Api-Key", apiKey);
		return headers;
	}

	public <T, S> GeneralResponse<T> executeRequest(HttpMethod httpMethod, String path, HttpEntity<S> entity,
			ParameterizedTypeReference<GeneralResponse<T>> reference, Object... urivar) {

		try {
			ResponseEntity<GeneralResponse<T>> response = this.restTemplate.exchange(path, httpMethod, entity,
					reference, urivar);
			if (response.getStatusCode() == HttpStatus.OK) {
				return response.getBody();
			} else {
				return null;
			}

		} catch (HttpClientErrorException ex) {
			log.error(" StatusCode: {} - Message: {}", ex.getRawStatusCode(), ex.getMessage());
		} catch (HttpServerErrorException ex) {
			log.error(" StatusCode: {} - Message: {}", ex.getRawStatusCode(), ex.getMessage());
		} catch (RestClientException ex) {
			log.error(" Message: {}", ex.getMessage());
		} catch (Exception ex) {
			log.error(" Message: {}", ex.getMessage());
		}

		return null;

	}

	
	public abstract RES executeService(REQ req);

}
