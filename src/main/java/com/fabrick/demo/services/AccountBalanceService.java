package com.fabrick.demo.services;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fabrick.demo.models.AccountBalanceRequest;
import com.fabrick.demo.models.AccountBalanceResponse;
import com.fabrick.demo.models.cash.AccountBalancePayload;
import com.fabrick.demo.models.common.GeneralResponse;

@Service
public class AccountBalanceService extends FabrickBaseService<AccountBalanceRequest, AccountBalanceResponse> {

	public AccountBalanceService(RestTemplateBuilder restTemplateBuilder) {
		super(restTemplateBuilder);
	}

	@Override
	public AccountBalanceResponse executeService(AccountBalanceRequest req) {
		
		HttpHeaders headers = setHeaders();
		HttpEntity<String> entity = new HttpEntity<>(headers);
		String balancePath = getBalancePath();
		long id = req.getIdAccount();
		GeneralResponse<AccountBalancePayload> response = executeRequest(HttpMethod.GET, balancePath, entity,
				new ParameterizedTypeReference<GeneralResponse<AccountBalancePayload>>() {
				}, id);
		if (response != null) {
			return new AccountBalanceResponse(response);
		} else {
			return null;
		}
		
	}

}
