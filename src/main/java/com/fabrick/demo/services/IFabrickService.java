package com.fabrick.demo.services;

import com.fabrick.demo.models.AccountBalanceRequest;
import com.fabrick.demo.models.AccountBalanceResponse;
import com.fabrick.demo.models.AccountTransactionsRequest;
import com.fabrick.demo.models.AccountTransactionsResponse;
import com.fabrick.demo.models.CreateMoneyTransferRequest;
import com.fabrick.demo.models.CreateMoneyTransferResponse;

public interface IFabrickService {
	
	public AccountBalanceResponse getAccountBalance(AccountBalanceRequest req);
	public AccountTransactionsResponse getAccountTransaction(AccountTransactionsRequest req);
	public CreateMoneyTransferResponse postCreateMoneyTransfer(CreateMoneyTransferRequest req);
	

}
