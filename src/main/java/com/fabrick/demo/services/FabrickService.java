package com.fabrick.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabrick.demo.models.AccountBalanceRequest;
import com.fabrick.demo.models.AccountBalanceResponse;
import com.fabrick.demo.models.AccountTransactionsRequest;
import com.fabrick.demo.models.AccountTransactionsResponse;
import com.fabrick.demo.models.CreateMoneyTransferRequest;
import com.fabrick.demo.models.CreateMoneyTransferResponse;

@Service
public class FabrickService implements IFabrickService {
	
	@Autowired
	private AccountBalanceService accountBalanceService;
	@Autowired
	private TransactionsService accountTransactionService;
	@Autowired
	private CreateMoneyTransferService createMoneyTransferService;

	@Override
	public AccountBalanceResponse getAccountBalance(AccountBalanceRequest req) {
		return accountBalanceService.executeService(req);
	}

	@Override
	public AccountTransactionsResponse getAccountTransaction(AccountTransactionsRequest req) {
		return accountTransactionService.executeService(req);
	}

	@Override
	public CreateMoneyTransferResponse postCreateMoneyTransfer(CreateMoneyTransferRequest req) {
		return createMoneyTransferService.executeService(req);
	}

}
