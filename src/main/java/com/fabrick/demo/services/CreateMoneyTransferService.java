package com.fabrick.demo.services;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fabrick.demo.models.CreateMoneyTransferRequest;
import com.fabrick.demo.models.CreateMoneyTransferResponse;
import com.fabrick.demo.models.common.GeneralResponse;
import com.fabrick.demo.models.payments.CreateMoneyTransferPayload;
import com.fabrick.demo.models.payments.CreateMoneyTransferRqBody;
import com.fabrick.demo.models.payments.Creditor;
import com.fabrick.demo.models.payments.TaxRelief;

@Service
public class CreateMoneyTransferService
		extends FabrickBaseService<CreateMoneyTransferRequest, CreateMoneyTransferResponse> {

	public CreateMoneyTransferService(RestTemplateBuilder restTemplateBuilder) {
		super(restTemplateBuilder);
	}

	@Override
	public CreateMoneyTransferResponse executeService(CreateMoneyTransferRequest req) {

		HttpHeaders headers = setHeaders();
		headers.set("X-Time-Zone", req.getHeaderXTimeZone());
		
		Creditor creditor = req.getBody().getCreditor();
		String executionDate = req.getBody().getExecutionDate();
		String uri = req.getBody().getUri();
		String description = req.getBody().getDescription();
		String amount = req.getBody().getAmount();
		String currency = req.getBody().getCurrency();
		Boolean isUrgent = req.getBody().getIsUrgent();
		Boolean isInstant = req.getBody().getIsInstant();
		String feeType = req.getBody().getFeeType();
		String feeAccountId = req.getBody().getFeeAccountId();
		TaxRelief taxRelief = req.getBody().getTaxRelief();
		
		CreateMoneyTransferRqBody request = new CreateMoneyTransferRqBody(creditor, executionDate, uri, description, amount, currency, isUrgent, isInstant, feeType, feeAccountId, taxRelief);

		long id = req.getIdAccount();

		HttpEntity<CreateMoneyTransferRqBody> entity = new HttpEntity<>(request, headers);

		String moneyTransferPath = getmoneyTransferPath();
		GeneralResponse<CreateMoneyTransferPayload> response = executeRequest(HttpMethod.POST, moneyTransferPath,
				entity, new ParameterizedTypeReference<GeneralResponse<CreateMoneyTransferPayload>>() {
				}, id);
		if (response != null) {
			return new CreateMoneyTransferResponse(response);
		} else {
			return null;
		}
	}

}
