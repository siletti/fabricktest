package com.fabrick.demo.services;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fabrick.demo.models.AccountTransactionsRequest;
import com.fabrick.demo.models.AccountTransactionsResponse;
import com.fabrick.demo.models.cash.AccountTransactionsPayload;
import com.fabrick.demo.models.common.GeneralResponse;

@Service
public class TransactionsService extends FabrickBaseService<AccountTransactionsRequest, AccountTransactionsResponse> {

	public TransactionsService(RestTemplateBuilder restTemplateBuilder) {
		super(restTemplateBuilder);
	}

	@Override
	public AccountTransactionsResponse executeService(AccountTransactionsRequest req) {
		HttpHeaders headers = setHeaders();
		HttpEntity<String> entity = new HttpEntity<>(headers);
		String transactionsPath = getTransactionsPath();
		long id = req.getAccountId();
		String  fromDate = req.getFromAccountingDate();
		String  toDate = req.getToAccountingDate();
		GeneralResponse<AccountTransactionsPayload> response = executeRequest(HttpMethod.GET, transactionsPath, entity,
				new ParameterizedTypeReference<GeneralResponse<AccountTransactionsPayload>>() {
				}, id, fromDate, toDate);
		if (response != null) {
			return new AccountTransactionsResponse(response);
		} else {
			return null;
		}
	}

}
