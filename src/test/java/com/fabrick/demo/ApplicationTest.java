package com.fabrick.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fabrick.demo.dbservice.AccountTransactionDbService;
import com.fabrick.demo.models.AccountBalanceRequest;
import com.fabrick.demo.models.AccountBalanceResponse;
import com.fabrick.demo.models.AccountTransactionsRequest;
import com.fabrick.demo.models.AccountTransactionsResponse;
import com.fabrick.demo.models.CreateMoneyTransferRequest;
import com.fabrick.demo.models.CreateMoneyTransferResponse;
import com.fabrick.demo.models.cash.AccountTransaction;
import com.fabrick.demo.models.payments.Account;
import com.fabrick.demo.models.payments.CreateMoneyTransferRqBody;
import com.fabrick.demo.models.payments.Creditor;
import com.fabrick.demo.services.IFabrickService;

@SpringBootTest
@Tag("Esercizio")
class ApplicationTest {

	private final static long ID_ACCOUNT = 14537780;

	@Autowired
	private IFabrickService restService;

	@Autowired
	private AccountTransactionDbService accountTransactionDbService;

	private Logger log = LoggerFactory.getLogger(Application.class);

	@Test
	void esercizio() throws IOException {

		log.info(" ========================================================= ");
		log.info(" ================== Esercizio Fabrick ==================== ");

		log.info(" ================== Operazione Lettura saldo ");
		AccountBalanceRequest balanceRequest = new AccountBalanceRequest(ID_ACCOUNT);
		AccountBalanceResponse accountBalanceResponse = restService.getAccountBalance(balanceRequest);
		if (accountBalanceResponse != null) {
			log.info("Balance Account No {}: {}", ID_ACCOUNT,
					accountBalanceResponse.getResponse().getPayload().getBalance());
		}

		log.info(" ================== Operazione Bonifico ");
		Creditor creditor = new Creditor("Alberto Siletti", new Account("IT24S0301503200000002824610", null), null);
		CreateMoneyTransferRqBody body = new CreateMoneyTransferRqBody(creditor, "2020-02-7", null, "Esercizio",
				"8.00", "EUR", null, null, null, null, null);
		String headerXTimeZone = "Europe/Rome";
		CreateMoneyTransferRequest transferRequest = new CreateMoneyTransferRequest(ID_ACCOUNT, headerXTimeZone, body);
		CreateMoneyTransferResponse createMoneyTransferResponse = restService.postCreateMoneyTransfer(transferRequest);
		if (createMoneyTransferResponse != null) {
			log.info("Response: {}", createMoneyTransferResponse.getResponse().getPayload());
		}

		log.info(" ================== Operazione Lettura Transazioni 2019");
		AccountTransactionsRequest transactionsRequest = new AccountTransactionsRequest(ID_ACCOUNT, "2019-01-01", "2019-12-31");
		AccountTransactionsResponse accountTransactionResponse = restService.getAccountTransaction(transactionsRequest);
		if (accountTransactionResponse != null) {
			log.info("Transactions Account No {}: ", ID_ACCOUNT);
			for (AccountTransaction accountTransaction : accountTransactionResponse.getResponse().getPayload().getList()) {
				log.info("Transaction: {} ", accountTransaction.getTransactionId());
				log.info("Scrittura su DB transactionId: {} ", accountTransaction.getTransactionId());
				accountTransactionDbService.save(accountTransaction);
			}
		}
		
		log.info(" ================== Operazione Lettura DB ");
		for (AccountTransaction accountTransaction : accountTransactionDbService.findAll()) {
			log.info(accountTransaction.toString());
		}
		log.info(" ========================================================= ");
		log.info(" ================== Fine Esercizio ");
		assertNotNull(accountBalanceResponse);
		assertNotNull(createMoneyTransferResponse);
		assertNotNull(accountTransactionResponse);

	}

}
