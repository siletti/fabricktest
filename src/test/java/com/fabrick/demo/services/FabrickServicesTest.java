package com.fabrick.demo.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.util.ResourceUtils;

import com.fabrick.demo.dbservice.AccountTransactionDbService;
import com.fabrick.demo.models.AccountBalanceRequest;
import com.fabrick.demo.models.AccountBalanceResponse;
import com.fabrick.demo.models.AccountTransactionsRequest;
import com.fabrick.demo.models.AccountTransactionsResponse;
import com.fabrick.demo.models.CreateMoneyTransferRequest;
import com.fabrick.demo.models.CreateMoneyTransferResponse;
import com.fabrick.demo.models.cash.AccountBalancePayload;
import com.fabrick.demo.models.cash.AccountTransaction;
import com.fabrick.demo.models.cash.AccountTransactionsPayload;
import com.fabrick.demo.models.cash.TransactionType;
import com.fabrick.demo.models.common.Error;
import com.fabrick.demo.models.common.GeneralResponse;
import com.fabrick.demo.models.payments.Account;
import com.fabrick.demo.models.payments.CreateMoneyTransferPayload;
import com.fabrick.demo.models.payments.CreateMoneyTransferRqBody;
import com.fabrick.demo.models.payments.Creditor;
import com.fabrick.demo.models.payments.TaxRelief;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@Tag("MockedServerTest")
public class FabrickServicesTest {

	@Autowired
	private AccountBalanceService accountBalanceService;

	@Autowired
	private TransactionsService transactionsService;

	@Autowired
	private CreateMoneyTransferService createMoneyTransferService;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private AccountTransactionDbService accountTransactionDbService;

	private MockRestServiceServer server;

	private final static long ID_ACCOUNT = 14537780;

	@Test
	public void accountBalanceServiceTest() throws IOException {
		server = MockRestServiceServer.bindTo(accountBalanceService.getRestTemplate()).build();
		String responseJson = objectMapper.writeValueAsString(new GeneralResponse<AccountBalancePayload>("OK",
				new ArrayList<Error>(), new AccountBalancePayload("2019-12-30", "29.64", "29.64", "EUR")));
		this.server
				.expect(requestTo("https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/" + ID_ACCOUNT + "/balance"))
				.andExpect(method(HttpMethod.GET)).andExpect(header("Content-Type", "application/json"))
				.andExpect(header("Auth-Schema", "S2S"))
				.andExpect(header("Api-Key", "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP"))
				.andRespond(withSuccess(responseJson, MediaType.APPLICATION_JSON));

		AccountBalanceRequest req = new AccountBalanceRequest(ID_ACCOUNT);
		AccountBalanceResponse response = accountBalanceService.executeService(req);
		assertNotNull(response);
		String balance = response.getResponse().getPayload().getBalance();
		assertEquals("29.64", balance);
	}

	@Test
	public void transactionsServiceTest() throws IOException {
		server = MockRestServiceServer.bindTo(transactionsService.getRestTemplate()).build();
		TransactionType transactionType = new TransactionType("GBS_TRANSACTION_TYPE", "GBS_TRANSACTION_TYPE_0023");
		List<AccountTransaction> accountTransactions = new ArrayList<>(Arrays.asList(
				new AccountTransaction("1331714087", "00000000273015", "2019-04-01", "2019-04-01", transactionType,
						"-800", "EUR", "BA JOHN DOE PAYMENT INVOICE 75/2017"),
				new AccountTransaction("1331714088", "00000000273015", "2019-04-01", "2019-04-01", transactionType,
						"-1", "EUR", "CO MONEY TRANSFER FEES")));
		String responseJson = objectMapper.writeValueAsString(new GeneralResponse<AccountTransactionsPayload>("OK",
				new ArrayList<Error>(), new AccountTransactionsPayload(accountTransactions)));
		this.server
				.expect(requestTo("https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/" + ID_ACCOUNT
						+ "/transactions?fromAccountingDate=2019-01-01&toAccountingDate=2019-12-01"))
				.andExpect(method(HttpMethod.GET)).andExpect(header("Content-Type", "application/json"))
				.andExpect(header("Auth-Schema", "S2S"))
				.andExpect(header("Api-Key", "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP"))
				.andRespond(withSuccess(responseJson, MediaType.APPLICATION_JSON));

		AccountTransactionsRequest req = new AccountTransactionsRequest(ID_ACCOUNT, "2019-01-01", "2019-12-01");
		AccountTransactionsResponse response = transactionsService.executeService(req);
		assertNotNull(response);

		AccountTransactionsPayload payload = response.getResponse().getPayload();
		List<AccountTransaction> list = payload.getList();
		AccountTransaction transaction0 = list.get(0);
		AccountTransaction transaction1 = list.get(1);
		assertEquals("1331714087", list.get(0).getTransactionId());
		assertEquals("1331714088", list.get(1).getTransactionId());
		// Test DB operations
		accountTransactionDbService.save(transaction0);
		accountTransactionDbService.save(transaction1);
		assertEquals("1331714087", accountTransactionDbService.findById("1331714087").getTransactionId());
		assertEquals("1331714088", accountTransactionDbService.findById("1331714088").getTransactionId());
		accountTransactionDbService.deleteById("1331714088");
		assertNull(accountTransactionDbService.findById("1331714088"));
	}

	@Test
	public void createMoneyTransferServiceTest() throws IOException {
		server = MockRestServiceServer.bindTo(createMoneyTransferService.getRestTemplate()).build();
		File file = ResourceUtils.getFile("classpath:demo_create_money_transfer_body.json");
		String jsonBody = "";
		if (file.exists()) {
			jsonBody = new String(Files.readAllBytes(file.toPath()));
		}
		String responseJson = objectMapper.writeValueAsString(new GeneralResponse<CreateMoneyTransferPayload>("OK",
				new ArrayList<Error>(), new CreateMoneyTransferPayload()));
		this.server
				.expect(requestTo("https://sandbox.platfr.io/api/gbs/banking/v4.0/accounts/" + ID_ACCOUNT
						+ "/payments/money-transfers"))
				.andExpect(method(HttpMethod.POST)).andExpect(header("Content-Type", "application/json"))
				.andExpect(header("Auth-Schema", "S2S"))
				.andExpect(header("Api-Key", "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP"))
				.andExpect(header("X-Time-Zone", "Europe/Rome")).andExpect(content().string(jsonBody))
				.andRespond(withSuccess(responseJson, MediaType.APPLICATION_JSON));
		
		String headerXTimeZone ="Europe/Rome";
		Creditor creditor = new Creditor("John Doe", new Account("IT23A0336844430152923804660", null), null);
		String executionDate = "2020-02-01";
		String uri = null;
		String description = "Payment invoice 75/2017";
		String amount = "800";
		String currency = "EUR";
		Boolean isUrgent = null;
		Boolean isInstant = null;
		String feeType = null;
		String feeAccountId = null;
		TaxRelief taxRelief = null;
		CreateMoneyTransferRqBody body = new CreateMoneyTransferRqBody(creditor, executionDate, uri, description, amount, currency, isUrgent, isInstant, feeType, feeAccountId, taxRelief);
		CreateMoneyTransferRequest request = new CreateMoneyTransferRequest(ID_ACCOUNT, headerXTimeZone, body);
		CreateMoneyTransferResponse createMoneyTransferResponse = createMoneyTransferService.executeService(request);
		assertNotNull(createMoneyTransferResponse);
	}

}
